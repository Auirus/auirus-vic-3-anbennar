﻿archmages = {
	texture = "gfx/interface/icons/pops_icons/archmages.dds"
	color = hsv{ 0.90 0.58 0.46 }
	start_quality_of_life = 20
	working_adult_ratio = 0.5
	wage_weight = 6
	paid_private_wage = no
	literacy_target = 0.8
	dependent_wage = 0.0	# per year
	unemployment = yes
	
	# 70% - 100% politically engaged
	political_engagement_base = 0.7
	political_engagement_literacy_factor = 0.3
	
	political_engagement_mult = {
		value = 1
		
		add = {
			desc = "POP_STARVATION"	
			
			if = {
				limit = { 
					standard_of_living < 5
				}
				value = 1
			}		
		}		
	}	
	
	qualifications_growth_desc = "MAGES_QUALIFICATIONS_DESC"
	qualifications = { #trying out academics qualifications; aristocrats quals seemed too low
		if = {
			limit = {
				is_peasant_under_serfdom = no 
				literacy_rate > 0.2
			}
			# baseline: ( literacy - 0.2 ) * 10 * ( 2 if accepted culture and religion), norm 6 for Literacy 50% and Accepted C/R
			add = {
				value = literacy_rate
				subtract = 0.2
				multiply = 20
				desc = "QUALIFICATIONS_LITERACY_FACTOR"
			}

			if = {
				limit = {
					pop_acceptance >= acceptance_status_5
				}
				multiply = {
					value = 2
					desc = "POTENTIALS_CULTURAL_RELIGIOUS_ACCEPTANCE"
				}
			}
			else_if = {
				limit = {
					pop_acceptance >= acceptance_status_4
				}
				multiply = {
					value = 1.5
					desc = "POTENTIALS_CULTURAL_RELIGIOUS_ACCEPTANCE"
				}
			}

			if = {
				limit = {
					OR = {
						is_pop_type = clerks
						is_pop_type = academics
						is_pop_type = aristocrats
						is_pop_type = clergymen
					}	
				}
				multiply = {
					value = 5
					desc = "QUALIFICATIONS_FAVORED_TYPE"
				}
			}
		}
		else = { #even random peasants *can* have magical potential
			value = {
				desc = "QUALIFICATIONS_PEASANTS_UNDER_SERFDOM"
				value = 1
			}
		}
	}
	
	portrait_age = {
		integer_range = {
			min = define:NPortrait|GRACEFUL_AGING_START
			max = define:NPortrait|GRACEFUL_AGING_END
		}
	}			
	portrait_pose = { value = 0 }			
	portrait_is_female = { always = yes }
}
