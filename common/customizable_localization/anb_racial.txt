﻿race_name = {
	type = culture
	random_valid = no

	text = {
		trigger = { 
			is_centaur = yes
		}

		localization_key = centaur_race_heritage
	}

	text = {
		trigger = { 
			is_dwarven = yes
		}

		localization_key = dwarven_race_heritage
	}

	text = {
		trigger = { 
			culture_is_elven = yes
		}

		localization_key = elven_race_heritage
	}

	text = {
		trigger = { 
			is_gnollish = yes
		}

		localization_key = gnollish_race_heritage
	}

	text = {
		trigger = { 
			is_gnomish = yes
		}

		localization_key = gnomish_race_heritage
	}

	text = {
		trigger = { 
			is_goblin = yes
		}

		localization_key = goblin_race_heritage
	}

	text = {
		trigger = { 
			is_halfling = yes
		}

		localization_key = halfling_race_heritage
	}

	text = {
		trigger = { 
			is_half_elven = yes
		}

		localization_key = half_elven_race_heritage
	}

	text = {
		trigger = { 
			is_half_orcish = yes
		}

		localization_key = half_orcish_race_heritage
	}

	text = {
		trigger = { 
			is_harimari = yes
		}

		localization_key = harimari_race_heritage
	}

	text = {
		trigger = { 
			is_harpy = yes
		}

		localization_key = harpy_race_heritage
	}

	text = {
		trigger = { 
			is_hobgoblin = yes
		}

		localization_key = hobgoblin_race_heritage
	}

	text = {
		trigger = { 
			is_human = yes
		}

		localization_key = human_race_heritage
	}

	text = {
		trigger = { 
			is_kobold = yes
		}

		localization_key = kobold_race_heritage
	}

	text = {
		trigger = { 
			is_lizardman = yes
		}

		localization_key = lizardfolk_race_heritage
	}

	text = {
		trigger = { 
			is_mechanim = yes
		}

		localization_key = mechanim_race_heritage
	}

	text = {
		trigger = { 
			is_ogre = yes
		}

		localization_key = ogre_race_heritage
	}

	text = {
		trigger = { 
			is_orcish = yes
		}

		localization_key = orcish_race_heritage
	}

	text = {
		trigger = { 
			is_ruinborn = yes
		}

		localization_key = ruinborn_race_heritage
	}

	text = {
		trigger = { 
			is_troll = yes
		}

		localization_key = troll_race_heritage
	}

	text = {
		localization_key = missing_race
	}
}