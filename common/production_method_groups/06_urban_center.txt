﻿pmg_amenities = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_market_stalls
		pm_market_squares
		pm_covered_markets
		pm_arcades
	}
}

pmg_street_lighting = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_street_lighting
		pm_gas_streetlights
		pm_electric_streetlights
		# pm_mood_enhancing_starlight # Anbennar
	}
}

pmg_public_transport = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_public_transport
		pm_public_trams
		pm_public_motor_carriages
	}
}

# pmg_urban_hexcraft = { # Anbennar
# 	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
# 	production_methods = {
# 		pm_artisan_magecraft # Anbennar
# 		pm_illusion_advertising # Anbennar
# 		pm_everyday_artificery # Anbennar
# 	}
# }

pmg_urban_clergy = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_arcane_state_clergy
		pm_arcane_free_urban_clergy
		pm_state_urban_clergy
		pm_free_urban_clergy
		pm_no_urban_clergy
	}
}

pmg_base_building_arts_academy = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_traditional_art
		pm_realist_art
		pm_photographic_art
		pm_film_art
	}
}

pmg_principle_freedom_of_movement_3 = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_principle_freedom_of_movement_3
		pm_freedom_of_movement_no_effect
	}
}

pmg_ownership_building_arts_academy = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_traditional_patronage
		pm_bourgeoisie_patronage
		pm_independent_artists
		pm_arcane_bard_college
	}
}

pmg_base_building_power_plant = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_hydroelectric_plant
		pm_coal-fired_plant
		pm_oil-fired_plant
		pm_damestear_reactor # Anbennar
	}
}

pmg_automation_power_plant = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = { 
		pm_no_automation # Anbennar
		pm_automata_laborers_power_plants # Anbennar
		pm_automata_machinists_power_plants # Anbennar
		pm_automata_laborers_power_plants_enforced # Anbennar
		pm_automata_machinists_power_plants_enforced # Anbennar
	}
}