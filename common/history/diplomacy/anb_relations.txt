﻿DIPLOMACY = {

	#Everyone in Escann + Anbennar dislikes magocratic demesne
	c:A30 = {
		set_relations = { country = c:A25 value = -30 }
		set_relations = { country = c:A27 value = -30 }
		set_relations = { country = c:A28 value = -30 }
		set_relations = { country = c:A29 value = -30 }
		set_relations = { country = c:A31 value = -30 }
		set_relations = { country = c:A12 value = 10 }	#Jazhkredu likes MD as they helped their creation
		set_relations = { country = c:A24 value = -50 }	#cos mages fucked newshire with magical farm famine
		set_relations = { country = c:A22 value = -30 }

		#Anbennar and Grombar
		set_relations = { country = c:A01 value = -50 }
		set_relations = { country = c:A10 value = -50 }
	}
	c:A30 = {	#Magocratic Demesne-Wyvernheart
		set_relations = { country = c:A26 value = 20 }
	}

	c:A22 = { #Ancardia-Jazhkredu
		set_relations = { country = c:A12 value = 20 }
	}
	

	c:A26 = { #Wyvernheart-Grombar
		set_relations = { country = c:A10 value = -50 }
	}

	c:A01 = { #Anbennar disliking other big dudes
		set_relations = { country = c:A02 value = -50 }
		set_relations = { country = c:A03 value = -50 }
	}
	
	c:A04 = { #NL supports screwing over the Hierarchy
		set_relations = { country = c:B60 value = 30 }
	}

	c:A01 = { #Anbennar-Escanni relation
		set_relations = { country = c:A25 value = -50 }
		set_relations = { country = c:A26 value = -50 }
		set_relations = { country = c:A27 value = -30 }
		set_relations = { country = c:A28 value = -30 }
		set_relations = { country = c:A29 value = -30 }
		set_relations = { country = c:A30 value = -50 }
		set_relations = { country = c:A12 value = -30 }
	}

	c:A02 = {	#Vivin-Ravelian
		set_relations = { country = c:A33 value = 20 }
	}

	c:A09 = {	#Busilar
		set_relations = { country = c:F02 value = 30 } #Surakesh
		set_relations = { country = c:L01 value = -50 } #Konolkhatep
	}


	c:A27 = { #Blademarches-Rosande-Marrhold Three Kings Alliance
		set_relations = { country = c:A28 value = 30 }
		set_relations = { country = c:A29 value = 30 }
	}
	c:A28 = { #Blademarches-Rosande-Marrhold Three Kings Alliance
		set_relations = { country = c:A29 value = 30 }
	}

	c:A05 = { #Bisan likes their former overlord Anbennar
		set_relations = { country = c:A01 value = 20 }
	}

	c:A20 = { #Ibevar doesnt like Vivins
		set_relations = { country = c:A02 value = -20 }
	}

	c:A04 = { #Northern League-Grombar
		set_relations = { country = c:A10 value = -50 }
		set_relations = { country = c:B98 value = 30 } #NL policy to contain Lorent/GH
		set_relations = { country = c:B07 value = 30 } #NL policy to contain Lorent/GH
	}

	c:B98 = { 
		set_relations = { country = c:B21 value = 20 } #Trollsbay supported Ynnsmouth
		set_relations = { country = c:B07 value = 30 } #Good relations with the other big Aelantir republic
	}

	c:B42 = { #South Expanse alliance
		set_relations = { country = c:B34 value = 50 } #Former anti-Plumstead bloc
		set_relations = { country = c:B41 value = 30 } #Plumstead did a heel turn (still dislikes Beggaston though)
	}

	c:B41 = { #Plumstead-Beggaston rivalry
		set_relations = { country = c:B34 value = -30 } #Former oppressor
	}

	c:B29 = { #Sarda Empire
		set_relations = { country = c:B49 value = -30 } #Main rival
		set_relations = { country = c:B36 value = 30 } #Anti-Havoric bloc
		set_relations = { country = c:B31 value = 30 } #Anti-Havoric bloc
		set_relations = { country = c:B30 value = 50 } #Very loyal
		set_relations = { country = c:B33 value = -30 } #Resents Sarda
		set_relations = { country = c:B46 value = -15 } #Was just forced to concede Vels Bacar dam
	}

	c:B46 = { #Dragon Dominion
		set_relations = { country = c:B52 value = -50 } #Actively planning an invasion
		set_relations = { country = c:B38 value = -25 } #They're next
		set_relations = { country = c:B47 value = 20 } #Common aldanist policies under Nestrin
	}

	c:B35 = { #New Havoral is a pariah
		set_relations = { country = c:B46 value = -10 }
		set_relations = { country = c:B29 value = -50 }
		set_relations = { country = c:B31 value = -50 }
		set_relations = { country = c:B36 value = -25 }
		set_relations = { country = c:B33 value = -25 }
		set_relations = { country = c:B32 value = 20 } #Those two fought together in the end
	}

	c:B53 = { #Freemarches
		set_relations = { country = c:A06 value = 30 } #Gave the Bondbreakers
	}

	c:B27 = { #Neratica disliked by former enemies
		set_relations = { country = c:B09 value = -20 }
		set_relations = { country = c:B26 value = -20 }
	}

	c:B60 = {
		set_relations = { country = c:A06 value = -50 } #Evil artificery was revealed
		set_relations = { country = c:A04 value = 10 } #Sligh NL support
	}

	c:B05 = { #VG
		set_relations = { country = c:B07 value = 30 } #Artificer ties
		set_relations = { country = c:B82 value = -20 } #Eordan invader
		set_relations = { country = c:B85 value = -20 } #Eordan invader
		set_relations = { country = c:B84 value = -20 } #Eordan invader
		set_relations = { country = c:B03 value = -10 } #Trade rivals
	}

	c:B82 = {
		set_relations = { country = c:B81 value = 30 }  #Autumnal League
		set_relations = { country = c:B83 value = 30 }  #Autumnal League
		set_relations = { country = c:B85 value = 15 } #Remnant of the Four Seasons Alliance
		set_relations = { country = c:B84 value = 15 } #Remnant of the Four Seasons Alliance
		set_relations = { country = c:A06 value = -30 } #Recent invader
		set_relations = { country = c:B80 value = -50 } #Recent invader
	}

	c:B85 = {
		set_relations = { country = c:B84 value = 15 } #Remnant of the Four Seasons Alliance
		set_relations = { country = c:A06 value = -30 } #Recent invader
		set_relations = { country = c:B80 value = -50 } #Recent invader
	}

	c:B84 = {
		set_relations = { country = c:B85 value = 15 } #Remnant of the Four Seasons Alliance
		set_relations = { country = c:A06 value = -30 } #Recent invader
		set_relations = { country = c:B80 value = -50 } #Recent invader
	}

	c:B16 = { #Obaithail civil war
		set_relations = { country = c:B72 value = -30 }
	}

	c:B07 = {
		set_relations = { country = c:Y03 value = 20 } #Supported during Rending
		set_relations = { country = c:B76 value = 20 }
		set_relations = { country = c:C30 value = 20 }
		set_relations = { country = c:A06 value = -10 } #Bad historical relations
		set_relations = { country = c:C11 value = 10 }
	}
	
	c:C39 = { #Deyeion
		set_relations = { country = c:C37 value = 30 }
		set_relations = { country = c:C70 value = 30 }
	}
	c:C70 = { #Amgremos
		set_relations = { country = c:C37 value = 30 }
	}

	c:D02 = { #Krakdhumvror
		set_relations = { country = c:D05 value = 30 } #Amldihr
		set_relations = { country = c:A10 value = -50 } #Grombar
	}

	c:D05 = { #Amldihr
		set_relations = { country = c:D02 value = 30 } #Krakdhumvror
		set_relations = { country = c:D03 value = 50 } #Dur Vazhatun
	}
	c:D03 = { #Dur Vazhatun
		set_relations = { country = c:D05 value = 30 } #Amldihr
	}
	c:D06 = { #Kuxhezte
		set_relations = { country = c:D37 value = 30 } #Kuxhekre
	}
	c:D37 = { #Kuxhekre
		set_relations = { country = c:D06 value = 30 } #Kuxhezte
	}
	c:D12 = { #Hul Jorkad
		set_relations = { country = c:D14 value = 25 } #Ovdal Lodhum
		set_relations = { country = c:D15 value = -50 } #Obsidian Legion
	}
	c:D13 = { #Gor Burad
		set_relations = { country = c:D15 value = 50 } #Obsidian Legion
	}
	c:D14 = { #Ovdal Lodhum
		set_relations = { country = c:D12 value = 25 } #Hul Jorkad
		set_relations = { country = c:D15 value = -50 } #Obsidian Legion
	}
	c:D18 = { #Arg Ordstun
		set_relations = { country = c:D20 value = 25 } #Orlghelovar
		set_relations = { country = c:D15 value = -50 } #Obsidian Legion
	}
	c:D20 = { #Orlghelovar
		set_relations = { country = c:D18 value = 25 } #Arg Ordstun
		set_relations = { country = c:D15 value = -50 } #Obsidian Legion
	}
	c:D19 = { #Shazstundihr
		set_relations = { country = c:D15 value = -50 } #Obsidian Legion
	}

	c:D15 = { #Obsidian Legion
		set_relations = { country = c:D18 value = -50 } #Arg Ordstun
		set_relations = { country = c:D20 value = -50 } #Orlghelovar
		set_relations = { country = c:D19 value = -50 } #Shazstundihr
		set_relations = { country = c:D14 value = -50 } #Ovdal Lodhum
		set_relations = { country = c:D13 value = 50 } #Gor Burad
		set_relations = { country = c:D12 value = -50 } #Hul Jorkad
	}

	c:D26 = { #Nizhn Korvesto
		set_relations = { country = c:D31 value = -50 } #Dakaz Carzviya
		set_relations = { country = c:D33 value = -50 } #Zerzeko Radin
	}

	c:D31 = { #Dakaz Carzviya
		set_relations = { country = c:R07 value = 25 } #Nadiraji
		set_relations = { country = c:D26 value = -50 } #Nizhn Korvesto
		set_relations = { country = c:D33 value = -50 } #Zerzeko Radin
	}

	c:D33 = { #Zerzeko Radin
		set_relations = { country = c:D31 value = -50 } #Dakaz Carzviya
		set_relations = { country = c:D26 value = -50 } #Nizhn Korvesto
	}

	c:F01 = {	#Jaddanzar post Deioderan
		set_relations = { country = c:F02 value = -30 } 
		set_relations = { country = c:A09 value = -30 }
	}

	c:L01 = {	#Konolkhatep
		set_relations = { country = c:L18 value = 50 } #Haraagtseda 
		set_relations = { country = c:A09 value = -50 } #Busilar
		set_relations = { country = c:L13 value = -25 } #Goluztarr, should hate them more but gameplay
		set_relations = { country = c:L04 value = -25 } #Khasa, wants to conquer them
	}
	c:L17 = {	#Hakhepra
		set_relations = { country = c:L01 value = 25 } #Konolkhatep 
		set_relations = { country = c:L25 value = -50 } #Khatalashya
	}

	c:L18 = {	#Haraagtseda
		set_relations = { country = c:L01 value = 50 } #Konolkhatep
		set_relations = { country = c:L28 value = -25 } #Yezel Mora
	}
	c:L13 = {	#Golutzarr
		set_relations = { country = c:L01 value = -100 } #Konolkhatep
	}

	c:L04 = {	#Khasa
		set_relations = { country = c:L01 value = -50 } #Konolkhatep
		set_relations = { country = c:A09 value = -25 } #Busilar
	}

	c:L28 = {	#Yezel Mora
		set_relations = { country = c:L18 value = -50 } #Haraagtseda
		set_relations = { country = c:L27 value = -50 } #Melakmengi
		set_relations = { country = c:L25 value = -50 } #Khatalashya
	}

	c:L27 = {	#Melakmengi
		set_relations = { country = c:L28 value = -25 } #Yezel Mora
		set_relations = { country = c:L30 value = 25 } #Sky Soaring Eagle
		set_relations = { country = c:L29 value = 25 } #Naleni
	}
	c:L30 = {	#Sky Soaring Eagle
		set_relations = { country = c:L27 value = 25 } #Melakmengi
	}
	c:L29 = {	#Naleni
		set_relations = { country = c:L27 value = 25 } #Melakmengi
	}

	c:L26 = {	#Hisost Yamok
		set_relations = { country = c:L27 value = -25 } #Melakmengi
		set_relations = { country = c:L18 value = -25 } #Haraagtseda
	}

	c:L25 = {	#Khatalashya
		set_relations = { country = c:L28 value = -25 } #Yezel Mora
		set_relations = { country = c:L17 value = -25 } #Hakhepra
		set_relations = { country = c:L34 value = -25 } #Adzalan
	}

	c:L35 = {	#Karshyr
		set_relations = { country = c:A09 value = 25 } #Busilar
		set_relations = { country = c:L25 value = -25 } #Khatalashya
		set_relations = { country = c:L57 value = 50 } #Amiratsamo
		set_relations = { country = c:L36 value = -25 } #Nazhni
	}

	c:L36 = {	#Nazhni
		set_relations = { country = c:A09 value = 25 } #Busilar, drug trade intermediary
		set_relations = { country = c:L25 value = -25 } #Khatalashya
	}

	c:L34 = {	#Adzalan
		set_relations = { country = c:L31 value = 50 } #Thunder Wielding Buffalo
		set_relations = { country = c:L25 value = -25 } #Khatalashya
	}

	c:L31 = {	#Thunder Wielding Buffalo
		set_relations = { country = c:L34 value = 50 } #Adzalan
	}
}
