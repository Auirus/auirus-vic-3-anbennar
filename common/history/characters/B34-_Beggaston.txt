﻿CHARACTERS = {
	c:B34 ?= {
		create_character = { # ravelian folk preacher who’s leading the country through a rebirth of spiritualism, intellectualism, and religiosity, a ‘great awakening’ if you will. Perhaps seeks reconciliation with plumstead on the simple virtue of ‘both ravelian’
			first_name = Andrel
			last_name = Wickham
			historical = yes
			age = 29
			ruler = yes
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_theocrat
			traits = {
				charismatic imposing inspirational_orator
			}
		}

		create_character = { # Experienced general who earned both his fame and his position for his service in Beggaston breaking away from Plumstead after the fallout from the decades of the mountain. Background as a randomly drafted farmer who had a knack for warfare. Thinks democracy is inherently based and sees it as a means to help make Beggaston a more equal country. Is not very keen on reconciliation with plumstead. 
			first_name = Dominic
			last_name = Keen
			historical = yes
			age = 51
			interest_group = ig_rural_folk
			ig_leader = yes
			commander = yes
			ideology = ideology_liberal_leader 
			traits = {
				honorable surveyor tactful
			}
		}
	}
}
