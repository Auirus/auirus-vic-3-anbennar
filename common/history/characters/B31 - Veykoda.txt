﻿CHARACTERS = {
	c:B31 ?= {
		create_character = { #Last survivor of the Veykodan Guard, who raised Morvel
			first_name = Dharo
			last_name = Garforga
			historical = yes
			age = 56
			#ruler = yes
			interest_group = ig_landowners
			ig_leader = yes
			is_general = yes
			ideology = ideology_royalist
			traits = {
				persistent innovative honorable surveyor
			}
		}
	}
}
