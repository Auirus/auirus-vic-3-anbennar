﻿MILITARY_FORMATIONS = {
	c:F01 ?= { # Jaddanzar
		create_military_formation = {
			type = army
			hq_region = sr:region_far_bulwar
			name = "Suraelic Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_WEST_NAZA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_EAST_NAZA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_JADDANZAR
				count = 25
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_AVAMEZAN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_UPPER_SURAN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_AZKA_SUR
				count = 10
			}
			# save_scope_as = summer_vanguard
		}

		#create_character = {
		#	template = F01_general
		#	save_scope_as = B85_gen
		#}

		#scope:F01_gen = {
		#	transfer_to_formation = scope:summer_vanguard
		#}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_bulwar_proper
			name = "Suran Legion"

			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_LOWER_BURANUN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BULWAR
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_KUMARKAND
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SAD_SUR
				count = 20
			}
		}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_far_salahad
			name = "Salahad Legion"

			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_EBBUSUBTU
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_KERUHAR
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_ELAYENNA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MULEN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ARDU
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SAD_SUR
				count = 10
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_bulwar_proper
			name = "Divenhal Fleet"
		
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_LOWER_BURANUN
				count = 5
			}

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_LOWER_BURANUN
				count = 10
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_far_salahad
			name = "Eastern Fleet"
		
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_EBBUSUBTU
				count = 5
			}
		}
	}
	
	c:F02 ?= { # Surakesi League
		create_military_formation = {
			type = army
			hq_region = sr:region_bulwar_proper
			name = "League Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BRASAN
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_LOWER_SURAN
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DROLAS
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SAD_SUR
				count = 2
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_bulwar_proper
			name = "Brasan Fleet"
		
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_DROLAS
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_ANNAIL
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_BRASAN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_BRASAN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_IBTAT
				count = 5
			}
		}
	}
	
	c:F09 ?= { # Kuzaram
		create_military_formation = {
			type = army
			hq_region = sr:region_harpy_hills
			name = "Kuza's Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KUZARAM
				count = 8
			}
		}
	}
	
	c:F10 ?= { # Garlas Kel
		create_military_formation = {
			type = army
			hq_region = sr:region_harpy_hills
			name = "1st Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_GARLAS_KEL
				count = 5
			}
		}
	}
	
	c:F13 ?= { # Harpylen
		create_military_formation = {
			type = army
			hq_region = sr:region_harpy_hills
			name = "1st Winged Host"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_HARPYLEN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EAST_HARPY_HILLS
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_FIRANYALEN
				count = 5
			}
		}
	}
	
	c:F14 ?= { # Overclan
		create_military_formation = {
			type = army
			hq_region = sr:region_bahar
			name = "1st Overclan Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MEDBAHAR
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_MEGAIROUS
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_AQATBAHAR
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_BAHAR_PROPER
				count = 10
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_bahar
			name = "Reuyel Fleet"
		
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_REUYEL
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_CRATHANOR
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_AQATBAHAR
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_BAHAR_PROPER
				count = 5
			}
		}
	}

	c:F15 ?= { #Ovdal Tungr
		create_military_formation = {
			type = army
			hq_region = sr:region_bahar
			name = "Copper Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_OVDAL_TUNGR
				count = 5
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_bahar
			name = "Copper Merchant Navy"
		
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_OVDAL_TUNGR
				count = 20
			}
		}
	}
}