﻿POPS = {
	s:STATE_SUGROLTONA = {
		region_state:Y09 = {
			create_pop = {
				culture = beikling
				size = 500000
			}
			create_pop = {
				culture = horned_ogre
				size = 2280000
			}
		}
	}
	s:STATE_MORYOKANG = {
		region_state:Y09 = {
			create_pop = {
				culture = beikling
				size = 1370000
			}
			create_pop = {
				culture = horned_ogre
				size = 1970000
			}
		}
	}
	s:STATE_MOGUTIAN = {
		region_state:R19 = {
			create_pop = {
				culture = ikaniwagain
				size = 5440000
			}
			create_pop = {
				culture = serene_harimari
				size = 100000
			}
			create_pop = {
				culture = dragon_hobgoblin
				size = 3120000
			}
			create_pop = {
				culture = horned_ogre
				size = 200000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 1210000
			}
		}
	}
	s:STATE_BIANYUAN = {
		region_state:R19 = {
			create_pop = {
				culture = ikaniwagain
				size = 4760000
			}
			create_pop = {
				culture = serene_harimari
				size = 450000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 60000
			}
			create_pop = {
				culture = dragon_hobgoblin
				size = 770000
			}
			create_pop = {
				culture = feng_harpy
				size = 40000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 320000
			}
		}
	}
	s:STATE_EBYANFANGU = {
		region_state:R19 = {
			create_pop = {
				culture = ikaniwagain
				size = 11970000
			}
			create_pop = {
				culture = serene_harimari
				size = 1010000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 500000
			}
			create_pop = {
				culture = dragon_hobgoblin
				size = 7310000
			}
			create_pop = {
				culture = feng_harpy
				size = 130000
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 3780000
			}
		}
	}
	s:STATE_BALRIJIN = {
		region_state:R19 = {
			create_pop = {
				culture = beikling
				size = 330000
			}
			create_pop = {
				culture = serene_harimari
				size = 20000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 1100000
			}
			create_pop = {
				culture = feng_harpy
				size = 60000
			}
		}
	}
	s:STATE_YANSZIN = {
		region_state:R19 = {
			create_pop = {
				culture = jiangszun
				size = 13040000
			}
			create_pop = {
				culture = serene_harimari
				size = 2270000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 370000
			}
			create_pop = {
				culture = dragon_hobgoblin
				size = 1370000
			}
		}
		region_state:Y03 = {
			create_pop = {
				culture = jiangszun
				size = 4270000
			}
			create_pop = {
				culture = serene_harimari
				size = 230000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 770000
			}
		}
	}
	s:STATE_ZYUJYUT = {
		region_state:Y07 = {
			create_pop = {
				culture = naamjyut
				size = 2760000
			}
			create_pop = {
				culture = serene_harimari
				size = 20000
			}
		}
		region_state:Y06 = {
			create_pop = {
				culture = naamjyut
				size = 4150000
			}
			create_pop = {
				culture = serene_harimari
				size = 210000
			}
		}
		region_state:R19 = {
			create_pop = {
				culture = naamjyut
				size = 610000
			}
			create_pop = {
				culture = dragon_hobgoblin
				size = 80000
			}
		}
	}
	s:STATE_LUOYIP = {
		region_state:Y06 = {
			create_pop = {
				culture = gangim
				size = 8520000
				split_religion = {
					gangim = {
						righteous_path = 0.6
						high_philosophy = 0.4
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 480000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 570000
			}
		}
	}
	s:STATE_HUNGNGON = {
		region_state:Y05 = {
			create_pop = {
				culture = naamjyut
				size = 950000
				split_religion = {
					naamjyut = {
						ravelian = 0.5
						righteous_path = 0.5
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 10000
			}
		}
		region_state:A06 = {
			create_pop = {
				culture = naamjyut
				size = 1220000
			}
			create_pop = {
				culture = serene_harimari
				size = 30000
			}
		}
	}
	s:STATE_IONGSIM = {
		region_state:Y05 = {
			create_pop = {
				culture = gangim
				size = 9430000
				split_religion = {
					gangim = {
						righteous_path = 0.32
						high_philosophy = 0.30
						ravelian = 0.38
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 520000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 420000
			}
			create_pop = {
				culture = tinker_gnome
				size = 40000
			}
			create_pop = {
				culture = city_goblin
				size = 20000
			}
		}
	}
	s:STATE_LINGYUK = {
		region_state:Y04 = {
			create_pop = {
				culture = gangim
				size = 3990000
				religion = mystic_accord
			}
			create_pop = {
				culture = serene_harimari
				size = 920000
				religion = mystic_accord
			}
			create_pop = {
				culture = goldscale_kobold
				size = 200000
				religion = mystic_accord
			}
		}
	}
	s:STATE_LIUMINXIANG = {
		region_state:Y03 = {
			create_pop = {
				culture = jiangszun
				size = 7590000
				split_religion = {
					jiangszun = {
						high_philosophy = 0.30
						righteous_path = 0.5
						mystic_accord = 0.2
					}
				}

			}
			create_pop = {
				culture = serene_harimari
				size = 880000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 1020000
			}
		}
		region_state:Y11 = {
			create_pop = {
				culture = jiangszun
				size = 8300000
				split_religion = {
					jiangszun = {
						high_philosophy = 0.30
						righteous_path = 0.5
						mystic_accord = 0.2
					}
				}
			}
			create_pop = {
				culture = shuvuush
				size = 2100000
			}
			create_pop = {
				culture = serene_harimari
				size = 390000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 880000
			}
		}
	}
	s:STATE_TIANLOU = {
		region_state:Y03 = {
			create_pop = {
				culture = jiangszun
				size = 12370000
				split_religion = {
					jiangszun = {
						high_philosophy = 0.4
						mystic_accord = 0.6
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 1410000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 550000
			}
			create_pop = {
				culture = nephrite_dwarf
				size = 20000
			}
			create_pop = {
				culture = sun_elven
				size = 20000
			}
		}
		region_state:Y05 = {
			create_pop = {
				culture = jiangszun
				size = 640000
				split_religion = {
					jiangszun = {
						ravelian = 0.3
						high_philosphy = 0.2
						mystic_accord = 0.5
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 160000
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = jiangszun
				size = 890000
				religion = righteous_path
			}
			create_pop = {
				culture = serene_harimari
				size = 430000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 110000
			}
			create_pop = {
				culture = steelscale_kobold
				size = 10000
			}
			create_pop = {
				culture = soot_goblin
				size = 20000
			}
			create_pop = {
				culture = tinker_gnome
				size = 30000
			}
		}
	}
	s:STATE_JINJIANG = {
		region_state:Y03 = {
			create_pop = {
				culture = jiangszun
				size = 15180000
				split_religion = {
					jiangszun = {
						mystic_accord = 0.5
						righteous_path = 0.2
						high_philosophy = 0.3
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 2520000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 1220000
			}
		}
		region_state:Y04 = {
			create_pop = {
				culture = jiangszun
				size = 630000
				religion = mystic_accord
			}
			create_pop = {
				culture = serene_harimari
				size = 720000
				religion = mystic_accord
			}
		}
	}
	s:STATE_XUANBING = {
		region_state:Y08 = {
			create_pop = {
				culture = jiangyang
				size = 7080000
				split_religion = {
					jiangyang = {
						mystic_accord = 0.5
						righteous_path = 0.4
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 390000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 160000
			}
		}
		region_state:Y03 = {
			create_pop = {
				culture = jiangyang
				size = 1770000
				split_religion = {
					jiangyang= {
						mystic_accord = 0.5
						righteous_path = 0.4
						high_philosophy = 0.1
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 90000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 30000
			}
		}
	}
	s:STATE_JIANTSIANG = {
		region_state:Y11 = {
			create_pop = {
				culture = jiangyang
				size = 7729000
				split_religion = {
					jiangyang = {
						bangujonsi  = 0.4
						saanorgegh  = 0.3
						mystic_accord = 0.3
					}
				}
			}
			create_pop = {
				culture = shuvuush
				size = 3081000
				split_religion = {
					shuvuush = {
						bangujonsi  = 0.4
						saanorgegh  = 0.3
						mystic_accord = 0.3
					}
				}
			}
			create_pop = {
				culture = serene_harimari
				size = 4390000
				split_religion = {
					serene_harimari = {
						bangujonsi  = 0.4
						saanorgegh  = 0.3
						mystic_accord = 0.3
					}
				}
			}
			create_pop = {
				culture = goldscale_kobold
				size = 470000
				split_religion = {
					goldscale_kobold = {
						bangujonsi  = 0.4
						saanorgegh  = 0.3
						mystic_accord = 0.3
					}
				}
			}
		}
	}
	s:STATE_YUNGHUUN = {
		region_state:Y11 = {
			create_pop = {
				culture = shuvuush
				size = 4210000
			}
			create_pop = {
				culture = serene_harimari
				size = 90000
			}
			create_pop = {
				culture = goldscale_kobold
				size = 40000
			}
		}
	}
}