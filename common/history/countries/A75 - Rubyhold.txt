﻿COUNTRIES = {
	c:A75 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		effect_starting_politics_traditional = yes


		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_local_police
		activate_law = law_type:law_religious_schools
		# No health system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_magic
	}
}