﻿COUNTRIES = {
	c:L34 ?= {
		effect_starting_technology_tier_4_tech = yes
		effect_starting_artificery_tier_2_tech = yes

		add_technology_researched = academia #they have a university
		add_technology_researched = mandatory_service #prereq for line
		add_technology_researched = line_infantry #artifice, should have proper guns
		add_technology_researched = urban_planning #iron buildings
		add_technology_researched = romanticism #these lizards are all about art
		
		effect_starting_politics_traditional = yes

		# Laws
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_colonial_exploitation
		activate_law = law_type:law_no_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_no_workers_rights
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_legacy_slavery

		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_artifice
	}
}