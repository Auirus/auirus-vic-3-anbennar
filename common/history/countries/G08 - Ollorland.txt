﻿COUNTRIES = {
	c:G08 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		#These are just Noo Oddansbay's laws copy pasted
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_secret_police

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_frontier_colonization
		# No police
		# No schools
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_same_race_and_humans
		activate_law = law_type:law_nation_of_artifice

		set_institution_investment_level = { #The entire country's purpose is to break through the FotCO
			institution = institution_colonial_affairs
			level = 2
		}
	}
}