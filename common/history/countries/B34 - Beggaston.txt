﻿COUNTRIES = {
	c:B34 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = line_infantry
		
		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		#No colonies (too overstretched with other institutions)
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_charitable_health_system
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_poor_laws
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_heritage_only
		activate_law = law_type:law_nation_of_artifice
	}
}