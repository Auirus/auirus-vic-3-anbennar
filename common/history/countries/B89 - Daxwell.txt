﻿COUNTRIES = {
	c:B89 ?= {
		effect_starting_technology_tier_3_tech = yes

		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting #Democracy was subverted when Thilvis assumed direct control
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		# No colonial affairs
		# No police
		# No schools
		# No health system
		activate_law = law_type:law_nation_of_artifice
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_censorship
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_local_tolerance

		ig:ig_rural_folk = {
			add_ruling_interest_group = yes
		}
	}
}