﻿COUNTRIES = {
	c:L27 ?= {
		effect_starting_technology_tier_5_tech = yes
		
		#some extra techs
		add_technology_researched = prospecting #they have gold mines
		add_technology_researched = mandatory_service #constant wars
		add_technology_researched = centralization #unified mengi country
		



		effect_starting_politics_traditional = yes

		# Laws
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_national_guard

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_censorship
		activate_law = law_type:law_no_workers_rights
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery

		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_magic
	}
}