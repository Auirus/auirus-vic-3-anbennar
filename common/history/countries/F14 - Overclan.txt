﻿COUNTRIES = {
	c:F14 ?= {
		effect_starting_technology_tier_3_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion #Multiculturalism not possible without human rights
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_local_police
		#activate_law = law_type:law_religious_schools
		activate_law = law_type:law_homesteading
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice
	}
}