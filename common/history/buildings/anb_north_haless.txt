﻿BUILDINGS={
	s:STATE_UPPER_KHARUNYANA_BASIN = {
		region_state:Y39 = {
			create_building={ 
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=1
						region="STATE_UPPER_KHARUNYANA_BASIN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=2
						region="STATE_UPPER_KHARUNYANA_BASIN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=3
						region="STATE_UPPER_KHARUNYANA_BASIN"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=2
						region="STATE_UPPER_KHARUNYANA_BASIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:Y39"
						levels=1
						region="STATE_UPPER_KHARUNYANA_BASIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y39"
						levels=2
						region="STATE_UPPER_KHARUNYANA_BASIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:Y39"
						levels=1
						region="STATE_UPPER_KHARUNYANA_BASIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" }
			}
			create_building={
				building="building_artillery_foundries"
				add_ownership={
					country={
						country="c:Y39"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_cannons" "pm_automation_disabled" }
			}
		}
	}	
	
	S:STATE_GREENVALE = {
		region_state:Y39 = {
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=2
						region="STATE_GREENVALE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=3
						region="STATE_GREENVALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=7
						region="STATE_GREENVALE"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_sugar_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y39"
						levels=1
						region="STATE_GREENVALE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y39"
						levels=3
						region="STATE_GREENVALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" }
			}
			create_building={
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_furniture_manufacturies"
						country="c:Y39"
						levels=1
						region="STATE_GREENVALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_automation_disabled" "pm_no_luxuries" }
			}
		}
	}
	
	s:STATE_NOMSYULHANI_BADLANDS = {
		region_state:Y40 = {
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y40"
						levels=3
						region="STATE_NOMSYULHANI_BADLANDS"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y40"
						levels=2
						region="STATE_NOMSYULHANI_BADLANDS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_damestear_mine"
				add_ownership={
					building={
						type="building_damestear_mine"
						country="c:Y40"
						levels=1
						region="STATE_NOMSYULHANI_BADLANDS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_damestear_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:Y40"
						levels=1
						region="STATE_NOMSYULHANI_BADLANDS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_no_hardwood" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:Y40"
						levels=2
						region="STATE_NOMSYULHANI_BADLANDS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:Y40"
						levels=1
						region="STATE_NOMSYULHANI_BADLANDS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" }
			}
		}
	}
	
	s:STATE_HAKUURUK = {
		region_state:Y41 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y41"
						levels=2
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y41"
						levels=2
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y41"
						levels=2
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:Y41"
						levels=1
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_gem_mine"
				add_ownership={
					building={
						type="building_gem_mine"
						country="c:Y41"
						levels=1
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			#create_building={		#doesnt have the tech for this
			#	building="building_university"
			#	add_ownership={
			#		country={
			#			country="c:Y41"
			#			levels=1
			#		}
			#	}
			#	reserves=1
			#	activate_production_methods={ "pm_scholastic_education" "pm_secular_academia" }
			#}
		}	
		region_state:Y43 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y43"
						levels=1
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y43"
						levels=1
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:Y43"
						levels=1
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_crude_tools" "pm_automation_disabled" }
			}
			create_building={
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:Y43"
						levels=1
						region="STATE_HAKUURUK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_automation_disabled" "pm_no_luxuries" }
			}
		}	
	}
}	