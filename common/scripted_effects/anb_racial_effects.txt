﻿initialize_tolerance_group = {
	if = {
		limit = { NOT = { has_variable = tolerance_group } }
		#Gerudian
		if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = gerudian
						has_discrimination_trait = reachman
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:gerudian
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:gerudian_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:gerudian_tolerance_plus
			}
		}
		#Raheni
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = raheni
						has_discrimination_trait = wuhyun
						has_discrimination_trait = raheni_heritage
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:rahen
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:rahen_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:rahen_tolerance_plus
			}
		}
		#Bulwari
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = bulwari
						has_discrimination_trait = bulwari_heritage
						AND = {
							has_discrimination_trait = sun_elf
							NOT = { has_discrimination_trait = bom }
						}
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:bulwari
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:bulwari_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:bulwari_tolerance_plus
			}
		}
		#Sarhali
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = west_sarhaly_heritage
						has_discrimination_trait = east_sarhaly_heritage
						has_discrimination_trait = south_sarhaly_heritage
						has_discrimination_trait = kheteratan_trait
						has_discrimination_trait = akasi_trait
						has_discrimination_trait = fangaulan_trait
						has_discrimination_trait = vurebindu_trait
						has_discrimination_trait = irsukuban_trait
						has_discrimination_trait = mengi_trait
						has_discrimination_trait = inyaswarosan_trait
						has_discrimination_trait = baashidi_trait
						has_discrimination_trait = vyzemby_trait
						has_discrimination_trait = kitageztur_trait
						has_discrimination_trait = kitmakhara_trait
						has_discrimination_trait = sharkyatash_trait
						has_discrimination_trait = shartash_trait
						has_discrimination_trait = arratash_trait
						has_discrimination_trait = yaikva_trait
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:sarhali
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:sarhali_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:sarhali_tolerance_plus
			}
		}
		#Forbidden Plains
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = triunic
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:fp
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:fp_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:fp_tolerance_plus
			}
		}
		#Cannorian
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = anbennarian
						has_discrimination_trait = alenic
						has_discrimination_trait = businori
						has_discrimination_trait = escanni
						has_discrimination_trait = dostanorian
						has_discrimination_trait = lencori
						has_discrimination_trait = shirefoot
						has_discrimination_trait = moon_elven
						has_discrimination_trait = trollsbayer
						has_discrimination_trait = triarchic
						AND = {
							has_discrimination_trait = cannorian_heritage
							NOR = { 
								has_discrimination_trait = gerudian
								has_discrimination_trait = reachman
							}
						}
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:cannorian
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:cannorian_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:cannorian_tolerance_plus
			}
		}
		#Haless
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = northeast_halessi_heritage
						has_discrimination_trait = southeast_halessi_heritage
						has_discrimination_trait = yan
						has_discrimination_trait = kai
						has_discrimination_trait = bom
						has_discrimination_trait = yanglam
						has_discrimination_trait = odheongun
						has_discrimination_trait = khantaar
						has_discrimination_trait = besholgi
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:haless
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:haless_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:haless_tolerance_plus
			}
		}
		#Ruinborns
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = aelantiri_heritage
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:aelantiri
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:aelantiri_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:aelantiri_tolerance_plus
			}
		}
		#Dwarovar Dwarf
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = dwarven_race_heritage
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:dwarovar_dwarf
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:dwarovar_dwarf_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:dwarovar_dwarf_tolerance_plus
			}
		}
		#Dwarovar Blorc
		else_if = { 
			limit = {
				any_primary_culture = {
					OR = {
						has_discrimination_trait = black_orc_trait
					}
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:dwarovar_orc
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:dwarovar_orc_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:dwarovar_orc_tolerance_plus
			}
		}
		#Other Dwarovar get goblin
		else_if = { 
			limit = {
				OR = {
					capital.region = sr:region_northern_pass
					capital.region = sr:region_kings_rock
					capital.region = sr:region_mountainheart
					capital.region = sr:region_serpentreach
					capital.region = sr:region_segbandal
					capital.region = sr:region_tree_of_stone
					capital.region = sr:region_jade_mines
				}
			}
			set_variable = {
				name = tolerance_group
				value = flag:dwarovar_goblin
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:dwarovar_goblin_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:dwarovar_goblin_tolerance_plus
			}
		}
		#Cannorian overflow
		else = { 
			set_variable = {
				name = tolerance_group
				value = flag:cannorian
			}
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:cannorian_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:cannorian_tolerance_plus
			}
		}
	}
	else = {
		if = {
			limit = { has_law = law_type:law_all_heritage }
			add_primary_culture = cu:all_heritage_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:cannorian }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:cannorian_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:cannorian_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:bulwari }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:bulwari_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:bulwari_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:sarhali }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:sarhali_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:sarhali_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:fp }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:fp_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:fp_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:rahen }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:rahen_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:rahen_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:haless }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:haless_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:haless_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:gerudian }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:gerudian_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:gerudian_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:aelantiri }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:aelantiri_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:aelantiri_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_dwarf }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:dwarovar_dwarf_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:dwarovar_dwarf_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_goblin }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:dwarovar_goblin_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:dwarovar_goblin_tolerance_plus
			}
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_orc }
			if = {
				limit = { has_law = law_type:law_local_tolerance }
				add_primary_culture = cu:dwarovar_orc_tolerance
			}
			else_if = {
				limit = { has_law = law_type:law_expanded_tolerance }
				add_primary_culture = cu:dwarovar_orc_tolerance_plus
			}
		}
	}
}