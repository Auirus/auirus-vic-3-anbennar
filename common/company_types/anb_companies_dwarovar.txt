﻿#####################
####Northern Pass####
#####################

company_icesteel_cartel = {
	icon = "gfx/interface/icons/company_icons/basic_munitions.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_iron_mine
		building_steel_mills
	}

	potential = {
		has_interest_marker_in_region = region_northern_pass
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_KRAKDHUMVROR
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_KRAKDHUMVROR
			any_scope_building = {
				is_building_type = building_iron_mine
				level >= 10
			}
		}
		any_scope_state = {
			state_region = s:STATE_KRAKDHUMVROR
			any_scope_building = {
				is_building_type = building_steel_mills
				level >= 3
			}
		}
		has_technology_researched = bessemer_process
	}

	prosperity_modifier = {
		unit_army_offense_mult = 0.05
		country_magical_expertise_mult = 0.1
	}

	ai_weight = {
		value = 5
	}
}


#####################
#####King's Rock#####
#####################


company_vale_terrace = {
	icon = "gfx/interface/icons/company_icons/basic_food.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_farm_wheat.dds"

	flavored_company = yes

	building_types = {
		building_food_industry
		building_wheat_farm
	}

	potential = {
		has_interest_marker_in_region = region_kings_rock
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_WESTERN_VALE
			}
			any_scope_state = {
				state_region = s:STATE_EASTERN_VALE
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_WESTERN_VALE
			any_scope_building = {
				is_building_type = building_wheat_farm
				level >= 10
			}
		}
		any_scope_state = {
			state_region = s:STATE_EASTERN_VALE
			any_scope_building = {
				is_building_type = building_wheat_farm
				level >= 10
			}
		}
		has_technology_researched = intensive_agriculture
	}

	prosperity_modifier = {
		state_birth_rate_mult = 0.05
		state_food_security_add = 0.05
	}

	ai_weight = {
		value = 5
	}
}


#####################
###Mountain Heart####
#####################


company_massive_shaft = {
	icon = "gfx/interface/icons/company_icons/basic_mineral_mining.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"

	flavored_company = yes

	building_types = {
		building_iron_mine
		building_doodad_manufacturies
	}

	potential = {
		has_interest_marker_in_region = region_mountainheart
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_DEAD_CAVERNS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_DEAD_CAVERNS
			any_scope_building = {
				is_building_type = building_doodad_manufacturies
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		state_pollution_reduction_health_mult = 0.1
		building_group_bg_mining_throughput_add = 0.1
	}

	ai_weight = {
		value = 3
	}
}


company_dagrite_railforge = {
	icon = "gfx/interface/icons/company_icons/basic_electrics.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_motor_industry
		building_power_plant
	}

	potential = {
		has_interest_marker_in_region = region_mountainheart
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_ER_NATVIR
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_ER_NATVIR
			any_scope_building = {
				is_building_type = building_motor_industry
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		state_infrastructure_mult = 0.05
		state_harvest_condition_dwarovar_railway_disrepair_mult = -0.5
	}

	ai_weight = {
		value = 5
	}
}


company_sharkbite_solutions = {
	icon = "gfx/interface/icons/company_icons/basic_munitions.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_arms_industry
		building_mithril_mine
	}

	potential = {
		has_interest_marker_in_region = region_kings_rock
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_MITHRADHUM
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_MITHRADHUM
			any_scope_building = {
				is_building_type = building_arms_industry
				level >= 3
			}
		}
		has_technology_researched = rifling
	}

	prosperity_modifier = {
		building_group_bg_dwarovrod_throughput_add = 0.1
		state_radicalism_increases_violent_hostility_mult = -0.05
		state_radicalism_increases_cultural_erasure_mult = -0.05
		state_radicalism_increases_open_prejudice_mult = -0.05
	}

	ai_weight = {
		value = 5
	}
}


#####################
####Serpent Reach####
#####################

company_gromdhal_lapidary = {
	icon = "gfx/interface/icons/company_icons/basic_metalworks.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"

	flavored_company = yes

	building_types = {
		building_gem_mine
		building_glassworks
	}

	potential = {
		has_interest_marker_in_region = region_serpentreach
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_ARG_ORDSTUN
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_ARG_ORDSTUN
			any_scope_building = {
				is_building_type = building_gem_mine
				level >= 3
			}
			any_scope_building = {
				is_building_type = building_glassworks
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		country_minting_mult = 0.1
		country_prestige_mult = 0.1
	}

	ai_weight = {
		value = 5
	}
}


company_buradforge_armories = {
		icon = "gfx/interface/icons/company_icons/basic_munitions.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_arms_industry
		building_war_machine_industry
	}

	potential = {
		has_interest_marker_in_region = region_serpentreach
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_SCALDING_PITS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_SCALDING_PITS
			any_scope_building = {
				is_building_type = building_arms_industry
				level >= 3
			}
		}
		has_technology_researched = watertube_boiler
	}

	prosperity_modifier = {
		country_military_goods_cost_mult = -0.1
		country_military_tech_spread_mult = 0.05
	}

	ai_weight = {
		value = 5
	}
}

#####################
######Segbandal######
#####################

#####################
####Tree of Stone####
#####################



company_mit_mit = {
	icon = "gfx/interface/icons/company_icons/basic_chemicals.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_plantation.dds"

	flavored_company = yes

	building_types = {
		building_serpentbloom_farm
		building_opium_plantation
	}

	potential = {
		has_interest_marker_in_region = region_tree_of_stone
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_AMBER_MINES
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_AMBER_MINES
			any_scope_building = {
				is_building_type = building_opium_plantation
				level >= 5
			}
		}
	}

	prosperity_modifier = {
		unit_morale_recovery_mult = 0.1
		country_loyalists_from_legitimacy_mult = 0.1
	}

	ai_weight = {
		value = 5
	}
}


#####################
#####Jade Mines######
#####################

#####################
########Other########
#####################

company_basic_fungiculture = {
	icon = "gfx/interface/icons/company_icons/basic_agriculture_1.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_farm_rice.dds"
	
	uses_dynamic_naming = yes
	
	dynamic_company_type_names = {
		"dynamic_company_type_company"
		"dynamic_company_type_consortium"
		"dynamic_company_type_guild"
		"dynamic_company_type_society"
		"dynamic_company_type_fellowship"
	}
	
	building_types = {  
		building_mushroom_farm
		building_serpentbloom_farm	
	}

	possible = { 
		any_scope_state = {
			any_scope_building = {
				OR = {
					is_building_type = building_mushroom_farm
					is_building_type = building_serpentbloom_farm
				}
				level >= 3
			}
		}
	}
	
	prosperity_modifier = {
		state_harvest_condition_veladklufar_swarm_mult = -0.25
	}	
}


company_strongbellow_shipyard = {
	icon = "gfx/interface/icons/company_icons/basic_shipyards.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_harbor_shipbuilding.dds"

	flavored_company = yes

	building_types = {
		building_shipyards
		building_military_shipyards
		building_motor_industry
	}

	potential = {
		has_interest_marker_in_region = region_bahar
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_OVDAL_TUNGR
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_OVDAL_TUNGR
			any_scope_building = {
				is_building_type = building_motor_industry
				level >=4
			}
		}
	}

	prosperity_modifier = {
		country_convoys_capacity_mult = 0.1
		building_coal_mine_throughput_add = 0.05
	}

	ai_weight = {
		value = 3
	}
}


