﻿company_basic_damestear = {
	icon = "gfx/interface/icons/company_icons/basic_mineral_mining.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"
	
	uses_dynamic_naming = yes
	
	dynamic_company_type_names = {
		"dynamic_company_type_company"
		"dynamic_company_type_consortium"
		"dynamic_company_type_corporation"
		"dynamic_company_type_conglomerate"
		"dynamic_company_type_syndicate"
		"dynamic_company_type_combine"
		"dynamic_company_type_guild"
	}
	
	building_types = {
		building_damestear_mine
		#building_blueblood_plants
	}

	possible = { 
		any_scope_state = {
			any_scope_building = {
				OR = {
					is_building_type = building_damestear_mine
					#is_building_type = building_blueblood_plants
				}
				level >= 3
			}
		}
	}
	
	prosperity_modifier = {
		building_doodad_manufacturies_throughput_add = 0.1
	}
}

company_basic_mage_guild = {
	icon = "gfx/interface/icons/company_icons/basic_silk_and_dye.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_plantation.dds"
	
	uses_dynamic_naming = yes
	
	dynamic_company_type_names = {
		"dynamic_company_type_company"
		"dynamic_company_type_consortium"
		"dynamic_company_type_guild"
		"dynamic_company_type_society"
		"dynamic_company_type_fellowship"
	}
	
	building_types = {
		building_magical_reagents_workshop
	}

	possible = { 
		any_scope_state = {
			any_scope_building = {
				is_building_type = building_magical_reagents_workshop
				level >= 3
			}
		}
	}
	
	prosperity_modifier = {
		country_magical_expertise_mult = 0.05
	}	
}